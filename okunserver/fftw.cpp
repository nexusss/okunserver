#include "fftw.h"
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <cstring>

using std::cout;

//do calc
const float toDegree = 180.0 / M_PI;
const float maxA = 4194304; //(256 / 2) * 32768;

FFTW::FFTW()
{
    arraySize = channelSize * 2;

    sizeIn = 256 ;

    Li = new short[arraySize];
    Lq = new short[arraySize];
    Ui = new short[arraySize];
    Uq = new short[arraySize];
    Ri = new short[arraySize];
    Rq = new short[arraySize];
    Di = new short[arraySize];
    Dq = new short[arraySize];
}

void FFTW::init(){
//    fftw_make_planner_thread_safe();

    cOut = (fftw_complex*) fftw_malloc(sizeIn * sizeof(fftw_complex));
    cIn = (fftw_complex*) fftw_malloc(sizeIn * sizeof(fftw_complex));
//    fftw_plan_with_nthreads(8);
    plan = fftw_plan_dft_1d(sizeIn,cIn,cOut,FFTW_FORWARD,FFTW_ESTIMATE);

}



void FFTW::setSize(const short &sizeIn){

    if(this->sizeIn != sizeIn && sizeIn != 0){

        fftw_destroy_plan(plan);

        fftw_free(cIn);
        fftw_free(cOut);


//        fftw_make_planner_thread_safe();

        this->sizeIn = sizeIn;

        cOut = (fftw_complex*) fftw_malloc(sizeIn * sizeof(fftw_complex));
        cIn = (fftw_complex*) fftw_malloc(sizeIn * sizeof(fftw_complex));
//        fftw_plan_with_nthreads(8);
        plan = fftw_plan_dft_1d(sizeIn,cIn,cOut,FFTW_FORWARD,FFTW_ESTIMATE);


    }
}

inline float CalcPhase(double &x,double &y){
    return static_cast<float>(atan2(y,x) * toDegree);
}

void FFTW::convertToMaxPower(float *out){

    fftw_execute(plan);
    out[0] = -100;
//    const float maxA = (sizeIn / 2) * 4096;//32768;
//    const float maxA = (sizeIn / 2) * 32768;//32768;
    for(short i = 1; i < sizeIn; i++){
        float c = sqrt(cOut[i][0] * cOut[i][0] + cOut[i][1] * cOut[i][1]) / maxA;
        out[i] = 20 * log10( c );

        //out[i] =  sqrt(cOut[i][0] * cOut[i][0] + cOut[i][1] * cOut[i][1]) / 128.0;        
    }    
}

void FFTW::convertToMaxPower(float *out, float *phase){
    fftw_execute(plan);
    out[0] = -100;
//    const float maxA = (sizeIn / 2) * 4096;//32768;
    //32768;
    for(short i = 1; i < sizeIn; i++){
        float c = sqrt(cOut[i][0] * cOut[i][0] + cOut[i][1] * cOut[i][1]) / maxA;
        out[i] = 20 * log10( c );
        //out[i] =  sqrt(cOut[i][0] * cOut[i][0] + cOut[i][1] * cOut[i][1]) / 128.0;
        if(out[i] >= treshold)
            phase[i] = CalcPhase(cOut[i][1],cOut[i][0]);
        else
            phase[i] = -200;
    }
}

void FFTW::convertToPhase( float *phase){
    fftw_execute(plan);
    for(short i = 1; i < sizeIn; i++){
        phase[i] = CalcPhase(cOut[i][1],cOut[i][0]);
    }
}

void FFTW::createPacketStruct(PacketStruct2 &packetStruct2){

    if(sizeIn == 0)
        return;

    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Li[i] + Ui[i] + Ri[i] + Di[i];
        cIn[i][1] = Lq[i] + Uq[i] + Rq[i] + Dq[i];
    }

    float sumphase[256];
    float hPh1[256],hPh2[256]; //horizont phase
    float vPh1[256],vPh2[256]; //horizont phase

    float ff[256];

    convertToMaxPower(ff,sumphase);
    memcpy(packetStruct2.data,ff,sizeof(ff));
    memcpy(packetStruct2.sumPhase,sumphase,sizeof(sumphase));


 //   packetStruct2.sumPhase = sumphase;


    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Li[i] + Ui[i] ;
        cIn[i][1] = Lq[i] + Uq[i];
    }

    convertToPhase(hPh1);

    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Ui[i] + Di[i];
        cIn[i][1] = Lq[i] + Dq[i];
    }

    convertToPhase(vPh1);

    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Ri[i] + Di[i];
        cIn[i][1] = Rq[i] + Dq[i];
    }


    convertToPhase(hPh2);

    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Li[i] + Ri[i];
        cIn[i][1] = Lq[i] + Rq[i];
    }

    convertToPhase(vPh2);

    for(int i = 0; i < 256; i++){
        if(packetStruct2.sumPhase[i] != -200){
            float dhPh1 = packetStruct2.sumPhase[i] - hPh1[i];
            float dhPh2 = packetStruct2.sumPhase[i] - hPh2[i];

            float dvPh1 = packetStruct2.sumPhase[i] - vPh1[i];
            float dvPh2 = packetStruct2.sumPhase[i] - vPh2[i];

            packetStruct2.horPhase[i] = (dhPh1 - dhPh2) / 2.0;
            packetStruct2.verPhase[i] = (dvPh1 - dvPh2) / 2.0;
        }
        else{
            packetStruct2.horPhase[i] = -200;
            packetStruct2.verPhase[i] = -200;
        }
    }

/*    float dhPh1 = packetStruct2.sumPhase - hPh1;
    float dhPh2 = packetStruct2.sumPhase - hPh2;

    float dvPh1 = packetStruct2.sumPhase - vPh1;
    float dvPh2 = packetStruct2.sumPhase - vPh2;

    packetStruct2.horPhase = (dhPh1 - dhPh2) / 2.0;
    packetStruct2.verPhase = (dvPh1 - dvPh2) / 2.0;*/

}

void FFTW::createPacketStruct(PacketStruct3 &packetStruct3){

    if(sizeIn == 0)
        return;

    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Li[i] + Ui[i] + Ri[i] + Di[i];
        cIn[i][1] = Lq[i] + Uq[i] + Rq[i] + Dq[i];
    }


    float sumphase[256];
    float hPh1[256],hPh2[256]; //horizont phase
    float vPh1[256],vPh2[256]; //horizont phase

    float ff[256];
    convertToMaxPower(ff,sumphase);
    memcpy(packetStruct3.data,ff,sizeof(ff));
    memcpy(packetStruct3.sumPhase,sumphase,sizeof(sumphase));

 //   packetStruct2.sumPhase = sumphase;


    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Li[i] + Ui[i] ;
        cIn[i][1] = Lq[i] + Uq[i];
    }

    convertToPhase(hPh1);

    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Ui[i] + Di[i];
        cIn[i][1] = Lq[i] + Dq[i];
    }

    convertToPhase(vPh1);

    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Ri[i] + Di[i];
        cIn[i][1] = Rq[i] + Dq[i];
    }


    convertToPhase(hPh2);

    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Li[i] + Ri[i];
        cIn[i][1] = Lq[i] + Rq[i];
    }


    convertToPhase(vPh2);

    for(int i = 0; i < 256; i++){
        if(packetStruct3.sumPhase[i] != -200){
            float dhPh1 = packetStruct3.sumPhase[i] - hPh1[i];
            float dhPh2 = packetStruct3.sumPhase[i] - hPh2[i];

            float dvPh1 = packetStruct3.sumPhase[i] - vPh1[i];
            float dvPh2 = packetStruct3.sumPhase[i] - vPh2[i];

            packetStruct3.horPhase[i] = (dhPh1 - dhPh2) / 2.0;
            packetStruct3.verPhase[i] = (dvPh1 - dvPh2) / 2.0;
        }
        else{
            packetStruct3.horPhase[i] = -200;
            packetStruct3.verPhase[i] = -200;
        }
    }

    //after 256

    for(int i = sizeIn,k = 0; i < sizeIn * 2; i++,k++){
        cIn[k][0] = Li[i] + Ui[i] + Ri[i] + Di[i];
        cIn[k][1] = Lq[i] + Uq[i] + Rq[i] + Dq[i];
    }

//    float sumphase;
//    float hPh1,hPh2; //horizont phase
//    float vPh1,vPh2; //horizont phase


    convertToMaxPower(ff,sumphase);
    memcpy(packetStruct3.data + 256,ff,sizeof(ff));
    memcpy(packetStruct3.sumPhase + 256,sumphase,sizeof(sumphase));

    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Li[i] + Ui[i] ;
        cIn[i][1] = Lq[i] + Uq[i];
    }

    convertToPhase(hPh1);

    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Ui[i] + Di[i];
        cIn[i][1] = Lq[i] + Dq[i];
    }

    convertToPhase(vPh1);

    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Ri[i] + Di[i];
        cIn[i][1] = Rq[i] + Dq[i];
    }


    convertToPhase(hPh2);

    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Li[i] + Ri[i];
        cIn[i][1] = Lq[i] + Rq[i];
    }

    convertToPhase(vPh2);

    for(int i = 0,k = 256; i < 256,k < 512; i++,k++){
        if(packetStruct3.sumPhase[k] != -200){
            float dhPh1 = packetStruct3.sumPhase[k] - hPh1[i];
            float dhPh2 = packetStruct3.sumPhase[k] - hPh2[i];

            float dvPh1 = packetStruct3.sumPhase[k] - vPh1[i];
            float dvPh2 = packetStruct3.sumPhase[k] - vPh2[i];

            packetStruct3.horPhase[k] = (dhPh1 - dhPh2) / 2.0;
            packetStruct3.verPhase[k] = (dvPh1 - dvPh2) / 2.0;
        }
        else{
            packetStruct3.horPhase[k] = -200;
            packetStruct3.verPhase[k] = -200;
        }
    }
}

void FFTW::createPacketStruct(PacketStruct4 &packetStruct4){
    if(sizeIn == 0)
        return;

    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Li[i];
        cIn[i][1] = Lq[i];
    }

    float ff[256];
    convertToMaxPower(ff);
    memcpy(packetStruct4.chL,ff,sizeof(ff));



    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Ui[i];
        cIn[i][1] = Uq[i];
    }

    convertToMaxPower(ff);
    memcpy(packetStruct4.chU,ff,sizeof(ff));


    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Ri[i];
        cIn[i][1] = Rq[i];
    }


    convertToMaxPower(ff);
    memcpy(packetStruct4.chR,ff,sizeof(ff));

    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Di[i];
        cIn[i][1] = Dq[i];
    }

    convertToMaxPower(ff);
    memcpy(packetStruct4.chD,ff,sizeof(ff));
}

void FFTW::createPacketStruct(PacketStruct5 &packetStruct5){
    if(sizeIn == 0)
        return;


    float phase;

    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Li[i];
        cIn[i][1] = Lq[i];
    }

    float ff[256];
    convertToMaxPower(ff);
    memcpy(packetStruct5.chL,ff,sizeof(ff));



    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Ui[i];
        cIn[i][1] = Uq[i];
    }

    convertToMaxPower(ff);
    memcpy(packetStruct5.chU,ff,sizeof(ff));


    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Ri[i];
        cIn[i][1] = Rq[i];
    }


    convertToMaxPower(ff);
    memcpy(packetStruct5.chR,ff,sizeof(ff));

    for(int i = 0; i < sizeIn; i++){
        cIn[i][0] = Di[i];
        cIn[i][1] = Dq[i];
    }

    convertToMaxPower(ff);
    memcpy(packetStruct5.chD,ff,sizeof(ff));

    //after 256


    for(int i = sizeIn,k = 0; i < sizeIn * 2; i++,k++){
        cIn[k][0] = Li[i];
        cIn[k][1] = Lq[i];
    }

    convertToMaxPower(ff);
    memcpy(packetStruct5.chL + 256,ff,sizeof(ff));



    for(int i = sizeIn,k = 0; i < sizeIn * 2; i++,k++){
        cIn[k][0] = Ui[i];
        cIn[k][1] = Uq[i];
    }

    convertToMaxPower(ff);
    memcpy(packetStruct5.chU + 256,ff,sizeof(ff));


    for(int i = sizeIn,k = 0; i < sizeIn * 2; i++,k++){
        cIn[k][0] = Ri[i];
        cIn[k][1] = Rq[i];
    }


    convertToMaxPower(ff);
    memcpy(packetStruct5.chR + 256,ff,sizeof(ff));

    for(int i = sizeIn,k = 0; i < sizeIn * 2; i++,k++){
        cIn[k][0] = Di[i];
        cIn[k][1] = Dq[i];
    }

    convertToMaxPower(ff);
    memcpy(packetStruct5.chD + 256,ff,sizeof(ff));
}

void FFTW::updateData(const unsigned short &dataStart){
    for(unsigned short i = dataStart,k = 0; i < data.size() && k < arraySize; i = i + readChannels,k++){

        Li[k] = data[i] - k0;
        Lq[k] = data[i + 1] - k1;
        Ui[k] = data[i + 2] - k2;
        Uq[k] = data[i + 3] - k3;
        Ri[k] = data[i + 4] - k4;
        Rq[k] = data[i + 5] - k5;
        Di[k] = data[i + 6] - k6;
        Dq[k] = data[i + 7] - k7;

//        Li[k] = data[i + leftChannel.I] - k0;
//        Lq[k] = data[i + leftChannel.Q] - k1;
//        Ui[k] = data[i + upChannel.I] - k2;
//        Uq[k] = data[i + upChannel.Q] - k3;
//        Ri[k] = data[i + rightChannel.I] -k4;
//        Rq[k] = data[i + rightChannel.Q] - k5;
//        Di[k] = data[i + downChannel.I] - k6;
//        Dq[k] = data[i + downChannel.Q] - k7;
    }
}

FFTW::~FFTW(){
    fftw_destroy_plan(plan);
    fftw_free(cIn);
    fftw_free(cOut);    
    delete [] Li;
    delete [] Lq;
    delete [] Ui;
    delete [] Uq;
    delete [] Ri;
    delete [] Rq;
    delete [] Di;
    delete [] Dq;
}

