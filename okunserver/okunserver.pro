TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -O3 -ffast-math -mfast-fp -mcpu=bf533

QMAKE_CFLAGS_RELEASE -= -O
QMAKE_CFLAGS_RELEASE -= -Os
QMAKE_CFLAGS_RELEASE -= -O1
QMAKE_CFLAGS_RELEASE -= -O2

QMAKE_CFLAGS_RELEASE *= -O3

LIBS += -pthread

SOURCES += main.cpp \
    perchtcpserver.cpp \
    client.cpp \
    data.cpp \
    fftw.cpp \
    settings.cpp \
    mutex.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    perchtcpserver.h \
    client.h \
    data.h \
    fftw.h \
    settings.h \
    mutex.h

unix:!macx: LIBS += -L$$PWD/bffftlib/lib/ -lfftw3

INCLUDEPATH += $$PWD/bffftlib/include
DEPENDPATH += $$PWD/bffftlib/include

unix:!macx: PRE_TARGETDEPS += $$PWD/bffftlib/lib/libfftw3.a

#unix:!macx: LIBS += -L$$PWD/bffftlib/lib/ -lfftw3f

#INCLUDEPATH += $$PWD/bffftlib/include
#DEPENDPATH += $$PWD/bffftlib/include

#unix:!macx: PRE_TARGETDEPS += $$PWD/bffftlib/lib/libfftw3f.a
