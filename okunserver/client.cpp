#include "client.h"

#include "mutex.h"
//#include <math_bf.h>
//#include <complex_bf.h>
//#include <filter.h>

struct sockaddr_in clientSockAddr;
int clientSock = -1, clientSlen = sizeof(clientSockAddr);

int softwareNumber = 0;




void createClientUdpSocket(in_addr srvip,unsigned short port){

//    complex_fract16 a,b,prod;
//    a.re = 1;
//    a.im = 0;
//    b.re = 8;
//    b.im = 0;
//    prod = cmul_fr32(a,b);
//    tw
    if ( (clientSock=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1){
        clientSock = -1;

    }

    memset((char *) &clientSockAddr, 0, sizeof(clientSockAddr));
    clientSockAddr.sin_family = AF_INET;
    clientSockAddr.sin_port = htons(port);
    clientSockAddr.sin_addr = srvip;    
}

void closeClientUDPSocket(){
    cout << "close udp socket\n";
    clientSock = -1;
    memset((char *) &clientSockAddr, 0, sizeof(clientSockAddr));

}

unsigned short findStartValue(){
    unsigned short retValue = 15, k = 0;
    for(retValue = 15, k = 0; k < data.size() / 2.0; retValue = retValue + readChannels, k++){
        if(data[retValue] == 1){
            break;
        }
    }
    return retValue - 15;
}

void *writeDataByUdpToClient(void *arg){


    PacketStruct0* packetStruct0 = new PacketStruct0;
    PacketStruct1* packetStruct1 = new PacketStruct1;
    PacketStruct2* packetStruct2 = new PacketStruct2;

    PacketStruct3* packetStruct3 = new PacketStruct3;
    PacketStruct4* packetStruct4 = new PacketStruct4;
    PacketStruct5* packetStruct5 = new PacketStruct5;

    FFTW *fftw = new FFTW();
    fftw->init();
    fftw->setSize(channelSize);


    packetStruct0->pktType = 0;
    packetStruct0->dataSize = 2048;

    packetStruct1->pktType = 1;
    packetStruct1->dataSize = 4096;


    packetStruct2->pktType = 2;
    packetStruct2->dataSize = 256;

    packetStruct3->pktType = 3;
    packetStruct3->dataSize = 512;

    packetStruct4->pktType = 4;
    packetStruct4->dataSize = 256;

    packetStruct5->pktType = 5;
    packetStruct5->dataSize = 512;

    while(1){

    pthread_mutex_lock(&upDownLock);

        if(clientSock != -1){


            switch (softwareNumber) {
            case 0:{
                if(up && down){

                    pthread_mutex_lock(&dataLock2);
                        unsigned short dataStart = findStartValue();
                    unsigned short size = packetStruct1->dataSize;
                    for(unsigned short i = dataStart,k = 0; k < size; i = i + readChannels,k = k + channels){
                        packetStruct1->data[k] = data[i] - k0;
                        packetStruct1->data[k + 1] = data[i + 1] - k1;
                        packetStruct1->data[k + 2] = data[i + 2] - k2;
                        packetStruct1->data[k + 3] = data[i + 3] - k3;
                        packetStruct1->data[k + 4] = data[i + 4] - k4;
                        packetStruct1->data[k + 5] = data[i + 5] - k5;
                        packetStruct1->data[k + 6] = data[i + 6] - k6;
                        packetStruct1->data[k + 7] = data[i + 7] - k7;

                    }

                    pthread_mutex_unlock(&dataLock2);



                    if (sendto(clientSock, (char*)packetStruct1 , sizeof(PacketStruct1) , 0 , (struct sockaddr *) &clientSockAddr, clientSlen)==-1){
                        cout << "erro send\n";
                    }
                }
                else{

                    pthread_mutex_lock(&dataLock2);
                        unsigned short dataStart = findStartValue();
                    unsigned short size = packetStruct0->dataSize;
                    for(unsigned short i = dataStart,k = 0; k < size; i = i + readChannels,k = k + channels){
                        packetStruct0->data[k] = data[i] - k0;
                        packetStruct0->data[k + 1] = data[i + 1] - k1;
                        packetStruct0->data[k + 2] = data[i + 2] - k2;
                        packetStruct0->data[k + 3] = data[i + 3] - k3;
                        packetStruct0->data[k + 4] = data[i + 4] - k4;
                        packetStruct0->data[k + 5] = data[i + 5] - k5;
                        packetStruct0->data[k + 6] = data[i + 6] - k6;
                        packetStruct0->data[k + 7] = data[i + 7] - k7;
                    }

                    pthread_mutex_unlock(&dataLock2);



                    if (sendto(clientSock, (char*)packetStruct0 , sizeof(PacketStruct0) , 0 , (struct sockaddr *) &clientSockAddr, clientSlen)==-1){
                        cout << "erro send\n";
                    }
                }
            }
                break;
            case 1:{
                pthread_mutex_lock(&dataLock2);
                    unsigned short dataStart = findStartValue();
                    fftw->updateData(dataStart);
                pthread_mutex_unlock(&dataLock2);
                if(up && down){
                    fftw->createPacketStruct(*packetStruct3);

                    if (sendto(clientSock, (char*)packetStruct3 , sizeof(PacketStruct3) , 0 , (struct sockaddr *) &clientSockAddr, clientSlen)==-1){
                        cout << "erro send\n";
                    }
                }
                else{
                    fftw->createPacketStruct(*packetStruct2);

                    if (sendto(clientSock, (char*)packetStruct2 , sizeof(PacketStruct2) , 0 , (struct sockaddr *) &clientSockAddr, clientSlen)==-1){
                        cout << "erro send\n";
                    }
                }
            }
                break;
            case 2:{
                pthread_mutex_lock(&dataLock2);
                    unsigned short dataStart = findStartValue();
                    fftw->updateData(dataStart);
                pthread_mutex_unlock(&dataLock2);

                if(up && down){
                    fftw->createPacketStruct(*packetStruct5);
                    if (sendto(clientSock, (char*)packetStruct5 , sizeof(PacketStruct5) , 0 , (struct sockaddr *) &clientSockAddr, clientSlen)==-1){
                        cout << "erro send\n";
                    }
                }
                else{
                    fftw->createPacketStruct(*packetStruct4);
                    if (sendto(clientSock, (char*)packetStruct4 , sizeof(PacketStruct4) , 0 , (struct sockaddr *) &clientSockAddr, clientSlen)==-1){
                        cout << "erro send\n";
                    }
                }
            }
                break;
            default:
                break;
            }
        }
        pthread_mutex_unlock(&upDownLock);
    }
    return 0;
}
