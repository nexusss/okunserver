#ifndef CLIENT_H
#define CLIENT_H

#include <netinet/in.h>
#include <arpa/inet.h>
#include "data.h"

#include <iostream>
#include <cstring>
#include <fftw.h>



using std::cout;
extern int softwareNumber;

void createClientUdpSocket(in_addr srvip, unsigned short port);
void closeClientUDPSocket();
void *writeDataByUdpToClient(void *arg);
#endif // CLIENT_H
