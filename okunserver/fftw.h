#ifndef FFTW_H
#define FFTW_H


#include <fftw3.h>
#include "data.h"

class FFTW
{

    short sizeIn;
    fftw_plan plan;
    fftw_complex *cOut, *cIn;

    short arraySize;
    short *Li;
    short *Lq;
    short *Ui;
    short *Uq;
    short *Ri;
    short *Rq;
    short *Di;
    short *Dq;

    void convertToMaxPower(float *out);
    void convertToMaxPower(float *out, float *phase);
    void convertToPhase( float *phase);

public:
    explicit FFTW();
    ~FFTW();
    void init();
    void setSize(const short &sizeIn);

    void createPacketStruct(PacketStruct2 &packetStruct2);
    void createPacketStruct(PacketStruct3 &packetStruct3);
    void createPacketStruct(PacketStruct4 &packetStruct4);
    void createPacketStruct(PacketStruct5 &packetStruct5);

    void updateData(const unsigned short &dataStart);

};


#endif // FFTW_H
