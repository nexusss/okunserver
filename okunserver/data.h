#ifndef DATA_H
#define DATA_H
#include <vector>
#include "settings.h"


#define LEFTCHANNELI "leftI\0"
#define LEFTCHANNELQ "leftQ\0"
#define UPCHANNELI "upI\0"
#define UPCHANNELQ "upQ\0"
#define RIGHTCHANNELI "rightI\0"
#define RIGHTCHANNELQ "rightQ\0"
#define DOWNCHANNELI "downI\0"
#define DOWNCHANNELQ "downQ\0"

#define TRESHOLD "treshold\0"
#define UPDOWN "updown\0"

struct Channel{
    char I;
    char Q;
};

template <typename T> union Convert{
    T val;
    char arr[sizeof(T)];
};

using std::vector;
extern vector<short> data;
extern vector<short> writedata;


extern const short channelSize;
extern const short channels;

extern const short readChannels;

extern const short readChannelUpAndDownSize;
extern const short readChannelUpOrDownSize;
extern bool upDownChanged;

extern const char serverVersion;

extern float treshold;

extern bool up;
extern bool down;


extern const char *configFileName;

extern configuration::data myconfigdata;

extern Channel leftChannel,rightChannel,upChannel,downChannel;

//extern pthread_mutex_t upDownLock;


extern const short k0,
            k1,
            k2,
            k3,
            k4,
            k5,
            k6,
            k7;



#pragma pack(push, 1)
struct PacketStruct0{
    char pktType;
    unsigned short dataSize;
    short data[2048];
};
struct PacketStruct1{
    char pktType;
    unsigned short dataSize;
    short data[4096];
};
#pragma pack(pop)
#pragma pack(push, 1)
struct PacketStruct2{
    char pktType;    
    unsigned short dataSize;
    float data[256];
    float sumPhase[256];
    float horPhase[256];
    float verPhase[256];
};
#pragma pack(pop)
#pragma pack(push, 1)
struct PacketStruct3{
    char pktType;
    unsigned short dataSize;
    float data[512];
    float sumPhase[512];
    float horPhase[512];
    float verPhase[512];
};
#pragma pack(pop)
#pragma pack(push, 1)
struct PacketStruct4{
    char pktType;
    unsigned short dataSize;
    float chL[256];
    float chU[256];
    float chR[256];
    float chD[256];
};
#pragma pack(pop)
#pragma pack(push, 1)
struct PacketStruct5{
    char pktType;
    unsigned short dataSize;
    float chL[512];
    float chU[512];
    float chR[512];
    float chD[512];
};
#pragma pack(pop)


#endif // DATA_H
