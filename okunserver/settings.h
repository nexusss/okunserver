#ifndef SETTINGS_H
#define SETTINGS_H


#include <iostream>
#include <map>
#include <string>
#include <sstream>

namespace configuration
  {

  //---------------------------------------------------------------------------
  // The configuration::data is a simple map string (key, value) pairs.
  // The file is stored as a simple listing of those pairs, one per line.
  // The key is separated from the value by an equal sign '='.
  // Commentary begins with the first non-space character on the line a hash or
  // semi-colon ('#' or ';').
  //
  // Example:
  //   # This is an example
  //   source.directory = C:\Documents and Settings\Jennifer\My Documents\
  //   file.types = *.jpg;*.gif;*.png;*.pix;*.tif;*.bmp
  //
  // Notice that the configuration file format does not permit values to span
  // more than one line, commentary at the end of a line, or [section]s.
  //
  struct data: std::map <std::string, std::string>
    {
    template <typename T> void getValue(T &data,const std::string &key,const std::string &defualt){
        if(count(key) == 0){
                insert(std::pair <std::string, std::string>(key,defualt));
                std::stringstream ss(defualt);
                ss >> data;
        }
        else{
            std::stringstream ss(at(key));
            ss >> data;
        }
    }
    };

  //---------------------------------------------------------------------------
  // The extraction operator reads configuration::data until EOF.
  // Invalid data is ignored.
  //
  std::istream& operator >> ( std::istream& ins, data& d );

  //---------------------------------------------------------------------------
  // The insertion operator writes all configuration::data to stream.
  //
  std::ostream& operator << ( std::ostream& outs, const data& d );


  } // namespace conf
#endif // SETTINGS_H
