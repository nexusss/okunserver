#include "perchtcpserver.h"

#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include "mutex.h"

#define MAX_SIZE 50
ip_config ipconfig;
enum  MESSAGETYPE{SOFTNUMBER = 1,UDP = 2,SETTINGSSET = 3};
void *startTCPServer(void *arg){

    int sport_fd = (int)arg;
    cout << "tcp sport fd " << sport_fd << "\n";
    int sock_descriptor, conn_desc;
    struct sockaddr_in serv_addr, client_addr;
    sock_descriptor = socket(AF_INET, SOCK_STREAM, 0);

    if(sock_descriptor < 0)
        cout << "Failed creating socket";
    else
        cout << "sock descrpit ok";

    // Initialize the server address struct to zero
    bzero((char *)&serv_addr, sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(serverport);



//    close(sock_descriptor);
cout << "start bind\n";
    int bindstatus = bind(sock_descriptor, (struct sockaddr *)&serv_addr, sizeof(serv_addr));

    cout << "bind " << bindstatus << "\n";

    if(bindstatus < 0){
        close(sock_descriptor);
        bindstatus = bind(sock_descriptor, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
    }

    cout << "bind " << bindstatus << "\n";


    listen(sock_descriptor, 1);

    while(1){
        cout << "Waiting for connection...\n";
        socklen_t size = sizeof(client_addr);

        conn_desc = accept(sock_descriptor, (struct sockaddr *)&client_addr, &size);
        cout << "accept\n";
        if (conn_desc == -1){
            cout << "Failed accepting connection";
        }
        else{

            cout << "Connected\n";

            /*ipconfig.sendIp = client_addr.sin_addr.s_addr;
                        cout << "ip " << ipconfig.sendIp;

                        if (ioctl(sport_fd, SPORT_IOC_SET_IP, &ipconfig) < 0)
                            cout  << "ioctl('%s', SPORT_IOC_SET_IP) failed" << sport_fd << "\n";
                        else
                            cout << "oictl ok\n";*/

            const short arrSize = 18;
            const short dataSize = arrSize - 2;
            char writebuf[arrSize];
            Convert<uint16_t> ds;
            ds.val = dataSize;

            Convert<float> tr;
            tr.val = treshold;

            writebuf[0] = ds.arr[0];
            writebuf[1] = ds.arr[1];
            writebuf[2] = CONNECT;
            writebuf[3] = serverVersion;
            writebuf[4] = softwareNumber;

            writebuf[5] = leftChannel.I;
            writebuf[6] = leftChannel.Q;
            writebuf[7] = upChannel.I;
            writebuf[8] = upChannel.Q;
            writebuf[9] = rightChannel.I;
            writebuf[10] = rightChannel.Q;
            writebuf[11] = downChannel.I;
            writebuf[12] = downChannel.Q;
            writebuf[13] = tr.arr[0];
            writebuf[14] = tr.arr[1];
            writebuf[15] = tr.arr[2];
            writebuf[16] = tr.arr[3];
            writebuf[17] = char(up << 1 | down);            

            write(conn_desc,writebuf,arrSize);


            bool socketwasclose = false;
            while(!socketwasclose){
                unsigned char buff[MAX_SIZE];
                // The new descriptor can be simply read from / written up just like a normal file descriptor
                if ( read(conn_desc, buff, sizeof(buff)-1) > 0){
                    Convert<uint16_t> ds;

                    ds.arr[0] = buff[0];
                    ds.arr[1] = buff[1];

                    short pktSize = ds.val;

                    MESSAGETYPE pkttype = static_cast<MESSAGETYPE>(buff[2]);
                    switch (pkttype) {
                    case SOFTNUMBER:
                        cout << "set fft status\n";
                        if(pktSize == 2){
                            softwareNumber = buff[3];
                            cout << "fft " << softwareNumber << "\n";
                            const short arrSize = 4;
                            const short dataSize = arrSize - 2;
                            char writebuf[arrSize];

                            Convert<uint16_t> wds;
                            wds.val = dataSize;

                            writebuf[0] = wds.arr[0];
                            writebuf[1] = wds.arr[1];
                            writebuf[2] = SOFTWARENUMBER;
                            writebuf[3] = softwareNumber;

                            write(conn_desc,writebuf,arrSize);
                        }
                        break;
                    case UDP:

                        if(pktSize == 3){

                            Convert<uint16_t> port;

                            port.arr[0] = buff[3];
                            port.arr[1] = buff[4];


                            createClientUdpSocket(client_addr.sin_addr,port.val);
//                            ipconfig.sendPort = buff[3] << 8 | buff[4];
                        }
                        break;
                    case SETTINGSSET:{
                        printf("settings set %d\n",pktSize);
                        if(pktSize == 14){
                            upDownChanged = true;
                            pthread_mutex_lock(&upDownLock);

//                            CanSetSettings.wait();

                                leftChannel.I = buff[3];
                                leftChannel.Q = buff[4];
                                upChannel.I = buff[5];
                                upChannel.Q = buff[6];
                                rightChannel.I = buff[7];
                                rightChannel.Q = buff[8];
                                downChannel.I = buff[9];
                                downChannel.Q = buff[10];

                                Convert<float> tr;
                                tr.arr[0] = buff[11];
                                tr.arr[1] = buff[12];
                                tr.arr[2] = buff[13];
                                tr.arr[3] = buff[14];

                                treshold = tr.val;


                                up = buff[15] & 0x2;
                                down = buff[15] & 0x1;



                                myconfigdata.at(LEFTCHANNELI) = leftChannel.I;
                                myconfigdata.at(LEFTCHANNELQ) = leftChannel.Q;

                                myconfigdata.at(UPCHANNELI) = upChannel.I;
                                myconfigdata.at(UPCHANNELQ) = upChannel.Q;

                                myconfigdata.at(RIGHTCHANNELI) = rightChannel.I;
                                myconfigdata.at(RIGHTCHANNELQ) = rightChannel.Q;

                                myconfigdata.at(DOWNCHANNELI) = downChannel.I;
                                myconfigdata.at(DOWNCHANNELQ) = downChannel.Q;

                                myconfigdata.at(TRESHOLD) = treshold;
                                myconfigdata.at(UPDOWN) = buff[15];

                                ofstream of(configFileName);
                                of << myconfigdata;
                                of.close();

                                short swset = 0;
                                if(up && down){
                                    swset = 0x00ff;
                                }
                                else if(up){
                                    swset = 0x00f0;
                                }
                                else if(down){
                                    swset = 0x000f;
                                }
                                else{
                                    swset = 0x00ff;
                                    up = true;
                                    down = true;
                                }

                                writedata[2] = swset;
                                writedata[3] = swset;

                                write(sport_fd, &writedata[0], writedata.size() * 2 );

                                printf("Settings set %d %d %d %d %d %d %d %d trehold %lf up %d %d %d\n",leftChannel.I,leftChannel.Q
                                       ,upChannel.I,upChannel.Q
                                       ,rightChannel.I,rightChannel.Q
                                       ,downChannel.I,downChannel.Q,
                                       treshold,up,down,swset);

                            pthread_mutex_unlock(&upDownLock);
                        }
                    }
                        break;
                    default:
                        cout << "default\n";
                        break;
                    }
                }
                else{
                    socketwasclose = true;                    
                    cout << "Failed receiving\n";
                }
            }
            cout << "close socket\n";
            closeClientUDPSocket();
            /*ipconfig.sendIp = 0;
                       cout << "ip " << ipconfig.sendIp;

                       if (ioctl(sport_fd, SPORT_IOC_SET_IP, &ipconfig) < 0)
                           cout  << "ioctl('%s', SPORT_IOC_SET_IP) failed" << sport_fd << "\n";
                       else
                           cout << "oictl ok\n";*/

            // Program should always close all sockets (the connected one as well as the listening one)
            // as soon as it is done processing with it
            close(conn_desc);
       }
    }
    cout << "close \n";
    close(sock_descriptor);

    return 0;
}
