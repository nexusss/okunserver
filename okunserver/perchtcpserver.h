#ifndef PERCHTCPSERVER_H
#define PERCHTCPSERVER_H

#include <iostream>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <asm/bfin_sport.h>
#include "client.h"

enum ANSWERTYPE{CONNECT = 10,SOFTWARENUMBER,SETTINGS};

const short serverport = 56421;
//const short clientport = 45454;
using namespace std;
void *startTCPServer(void *arg);

#endif // PERCHTCPSERVER_H
