#include <iostream>
#include "perchtcpserver.h"
#include <sstream>
#include <unistd.h>
#include <fstream>
#include <sys/ioctl.h>
#include <fcntl.h>
#include "mutex.h"

using namespace std;

void initConfig();

int main()
{

    if (pthread_mutex_init(&upDownLock, NULL) != 0){
        printf("\n mutex init failed\n");
        return 1;
    }

    if (pthread_mutex_init(&dataLock2, NULL) != 0){
        printf("\n mutex init failed\n");
        return 1;
    }

    initConfig();

    int result = 0;


    char *sport_path = "/dev/sport0";
    struct sport_config config;

    int sport_fd = open(sport_path, O_RDWR, 0);

    short readChannelSize;
    int wordsize;
    int dataSize;


    if(up == true && down == true){
        readChannelSize = readChannelUpAndDownSize;
    }
    else{
        readChannelSize = readChannelUpOrDownSize;
    }

    dataSize = readChannelSize * readChannels;
    data.resize(dataSize);
    writedata.resize(dataSize);
    wordsize = dataSize * sizeof(short);

    writedata[0] = 0x06f6;
    writedata[1] = 0x06f6;




    memset(&config, 0, sizeof(struct sport_config));
    config.mode = TDM_MODE;
    config.data_format = NORM_FORMAT;
    config.int_clk = 0;
    config.lsb_first = 0;

    config.act_low = 0;
    config.polled = 0;
    config.frame_delay = 0;
    config.fsync = 1;
    config.word_len = 12;
    config.dma_enabled = 1;
    config.channels = readChannels;
    config.wordsize = wordsize;

    cout << "main wordsize " << dataSize << " " << wordsize << "\n";

    if (sport_fd < 0)
        cout << "failed open " << sport_path << endl;
    else {
        cout << "succes open " << sport_path << endl;
    }
    /* Configure sport controller by ioctl */


    if (ioctl(sport_fd, SPORT_IOC_CONFIG, &config) < 0)
        cout  << "ioctl('%s', SPORT_IOC_CONFIG) failed" << sport_fd << endl;
    else
        cout << "oictl ok\n";




    pthread_t clientthread, tcpServerthread;
    printf("start create thread\n");

    result = pthread_create(&tcpServerthread, NULL, startTCPServer, (void*) sport_fd);
    if (result != 0) {
        cout << "server tcp thread failed";
        return result;
    }

    result = pthread_detach(tcpServerthread);
    if (result != 0) {
        cout << "wait tcp thread failed";
        return result;
    }

    result = pthread_create(&clientthread, NULL, writeDataByUdpToClient, NULL);
    if (result != 0) {
        cout << "client udp thread failed";
        return result;
    }

    result = pthread_detach(clientthread);
    if (result != 0) {
        cout << "wait clien thread failed";
        return result;
    }



    short swset = 0;
    if(up && down){
        swset = 0x00ff;
    }
    else if(up){
        swset = 0x00f0;
    }
    else if(down){
        swset = 0x000f;
    }
    else{
        swset = 0x00ff;
    }

    writedata[2] = swset;
    writedata[3] = swset;
    write(sport_fd, &writedata[0], writedata.size() * 2 );

    while(1){

        pthread_mutex_lock(&dataLock2);
            read(sport_fd, &data[0], wordsize );
        pthread_mutex_unlock(&dataLock2);


            if(upDownChanged){
                pthread_mutex_lock(&upDownLock);

                upDownChanged = false;

                if(up == true && down == true){
                    readChannelSize = readChannelUpAndDownSize;
                }
                else{
                    readChannelSize = readChannelUpOrDownSize;
                }
                dataSize = readChannelSize * readChannels;
                data.resize(dataSize);
                writedata.resize(dataSize);
                wordsize = dataSize * sizeof(short);
                config.wordsize = wordsize;

                if (ioctl(sport_fd, SPORT_IOC_CONFIG, &config) < 0)
                    cout  << "ioctl('%s', SPORT_IOC_CONFIG) failed" << sport_fd << endl;
                else
                    cout << "oictl ok\n";

                pthread_mutex_unlock(&upDownLock);
            }

    }
    cout << "close main thread\n";
    return 0;
}


void initConfig(){
    ifstream f(configFileName);
    f >> myconfigdata;

    myconfigdata.getValue(leftChannel.I,LEFTCHANNELI,"0");
    myconfigdata.getValue(leftChannel.Q,LEFTCHANNELQ,"1");

    myconfigdata.getValue(upChannel.I,UPCHANNELI,"2");
    myconfigdata.getValue(upChannel.Q,UPCHANNELQ,"3");

    myconfigdata.getValue(rightChannel.I,RIGHTCHANNELI,"4");
    myconfigdata.getValue(rightChannel.Q,RIGHTCHANNELQ,"5");

    myconfigdata.getValue(downChannel.I,DOWNCHANNELI,"6");
    myconfigdata.getValue(downChannel.Q,DOWNCHANNELQ,"7");

    myconfigdata.getValue(treshold,TRESHOLD,"-100");

    char sw = 0;
    myconfigdata.getValue(sw,UPDOWN,"3");
    up = sw & 0x2;
    down = sw & 0x1;

    printf("Settings set %d %d %d %d %d %d %d %d trehold %lf up %d %d \n",leftChannel.I,leftChannel.Q
           ,upChannel.I,upChannel.Q
           ,rightChannel.I,rightChannel.Q
           ,downChannel.I,downChannel.Q,
           treshold,up,down);


    f.close();

    ofstream of(configFileName);
    of << myconfigdata;
    of.close();
}
